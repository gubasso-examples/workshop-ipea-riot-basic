import reducer from './reducers/reducer'

const initialState = {
    header: ['Name', 'Favorite Color'],
    people: [
        {
            name: 'Bob',
            favcolor: 'Yellow'
        },
        {
            name: 'Michelle',
            favcolor: 'Purple'
        },
        {
            name: 'Ismael',
            favcolor: 'Rosinha bebe'
        }
    ]
}


const store = createStore(
    reducer,
    initialState
)

export default store

