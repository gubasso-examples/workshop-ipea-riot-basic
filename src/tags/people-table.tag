<people-table>
    <h3>People Table</h3>

    <table class="table table-bordered table-dark">
        <tr>
            <th each='{headername in opts.myheader}' scope="col">{headername}</th>
        </tr>
        <tr each='{person in opts.mypeople}'>
            <td each='{valor in person}'>{valor}</td>
        </tr>

    </table>

    <script>

        const minhaPiroca = this

        this.on('mount', function () {
            const linhas = document.querySelectorAll('tr')

            linhas.forEach((item) => {
                item.addEventListener('click', function () {
                    minhaPiroca.update()
                })
            })
        })

        this.on('update', function () {

        })
    </script>
</people-table>