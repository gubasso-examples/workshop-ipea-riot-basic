<app>

    <div class="container">


        <h1>People and their Favorite Colors!</h1>

        <!-- <form onsubmit={ add }>
            <input ref="input" onkeyup={ edit }>
            <button disabled={ !text }>Add #{ items.length + 1 }</button>
        </form> -->

        <form onsubmit='{handleSubmit}'>
            <input type="text" ref="name" placeholder="Name">
            <input type="text" ref="favcolor" placeholder="Favorite Color">
            <input type="submit" value="Ok, submit!">
        </form>

        <button onclick={handleDelete}>Delete Last One!</button>

        <div class="row">

            <people-list mypeople='{people}' class="col-sm" />

            <people-table myheader='{header}' mypeople='{people}' class="col-sm" />

        </div>

    </div>

    <script>
        this.mixin('store')


        this.people = this.getState().people
        this.header = this.getState().header

        // this.subscribe(() => {
        //     // this.update()
        // })

        this.on('update', function () {
            this.people = this.getState().people
            this.header = this.getState().header
        })

        this.handleSubmit = (e) => {
            e.preventDefault()

            const name = this.refs.name.value
            const favcolor = this.refs.favcolor.value

            this.dispatch({
                type: 'ADD_NEW_PERSON',
                payload: { name, favcolor }
            })

            // this.update()


        }

        this.handleDelete = () => {

            this.dispatch({
                type: 'DELETE_LAST_PERSON'
            })


        }



    </script>

</app>