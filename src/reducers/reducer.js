

const reducer = (state, action) => {

    switch (action.type) {

        case 'ADD_NEW_PERSON': {
            const newState = {
                header: state.header,
                people: [...state.people, action.payload]
            }
            return newState
        }
        case 'DELETE_LAST_PERSON': {

            state.people.pop()

            const newState = {
                header: state.header,
                people: state.people
            }
            return newState
        }

        default: return state

    }

}

export default reducer